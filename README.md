# vue2-ssr-bulma

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Run your unit tests
```
yarn test:unit
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### How to install or running
```
- copy
$ git clone https://gitlab.com/riefive87/vue2-ssr-bulma.git [project_name]
$ cd [project_name]
- initialized
$ yarn install 
$ yarn build
- optional run
$ yarn run:csr | yarn run:ssr
build production 
$ yarn start:production

### Rule config router
- Add sub routing with prefix name "route-[name].js"
- route-an-index.js is parent module of router
- ex: route-an-index.js

### Rule config store
- Add sub module at store with prefix name "store-[name].js"
- store-an-index.js is parent module of store
- ex: store-an-index.js, store-breakpoint.js

### Rule config plugins
- Add sub plugin with prefix name "plugin-[name].js"
- ex: plugin-breakpoint.js

### Rule config mixins
- Add sub mixin with prefix name "mixin-[name].js"
- ex: mixin-breakpoint.js

### Libraries
1. Util Object - instead of lodash
- helper for object manipulation
- reference [https://github.com/you-dont-need/You-Dont-Need-Lodash-Underscore]
- available method: get, empty, has, omit, pick, pull
2. Util String
- helper for text manipulation
- available method: capitalize, camelCase, kebabCase, snakeCase, etc...
3. Util Dom
- helper for dom [client side only]
- available method: screenOrientation, toggleFullScreen, userAgentInformation

### References
I. Server side Rendering
1. ssr.vuejs.org [http://ssr.vuejs.org]
- https://ssr.vuejs.org/guide/#installation
- https://ssr.vuejs.org/guide/structure.html
- https://ssr.vuejs.org/guide/routing.html
- https://ssr.vuejs.org/guide/bundle-renderer.html
- https://ssr.vuejs.org/guide/build-config.html
2. Moduslabs [https://labs.moduscreate.com/]
- https://www.youtube.com/watch?v=XJfaAkvLXyU&feature=youtu.be (Server side rendering with Vue.js 3)
- https://github.com/moduslabs/vue3-example-ssr
3. Namecheap [https://www.namecheap.com/]
- https://www.namecheap.com/blog/production-ready-vue-ssr-in-5-simple-steps/
- https://github.com/olegpisklov/vue-ssr-simple-setup

II. Breakpoint
1. Vuetify [https://vuetifyjs.com]
- https://vuetifyjs.com/en/features/breakpoints/

III. Layouting
1. Medium [https://medium.com]
- https://medium.com/@herujokoutomo/membuat-dynamic-layout-component-pada-vue-js-b8bef9b4eee