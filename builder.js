const fs = require('fs')
const path = require('path')
const cpx = require('cpx')
const rimraf = require('rimraf')
const packages = require('./package.json')

const outputName = '.appview'
const outputPath = `./${outputName}`
const cwd = process.cwd() || __dirname
const project = Object.assign({
    name: 'ssr-builder',
    description: 'Vue SSR Builder',
    version: '0.1.0',
    author: {
        name: 'riefive',
        email: 'rie.five@gmail.com'
    }
})

const scripts = {
    'start:client': 'export VUE_APP_TARGET=csr && node app',
    'start': 'export VUE_APP_TARGET=ssr && node app'
}
Object.assign(project, { scripts })

const dependKeys = Object.keys(packages.dependencies).filter(v => ['express', 'vue-server-renderer'].includes(v))
const dependencies = packages.dependencies
const dependenciesProduction = {}
for (const key in dependencies) {
    const element = dependencies[key]
    if (!dependKeys.includes(key)) { continue }
    Object.assign(dependenciesProduction, { [key]: element })
}

Object.assign(project, { dependencies: dependenciesProduction })
const filedir = path.resolve(cwd, outputName, 'package.json')
const mkdir = path.dirname(filedir)
const content = JSON.stringify(project, null, 4)
const readme = `
# Vue SSR
## To running app
- $ npm install --only=production
- $ export NODE_ENV=production && node app \n
## Options env
- development
- production
- staging
`.trim()

if (fs.existsSync(outputPath)) {
    rimraf(outputPath, (err) => { 
        if (err) { console.log(err) }
    })
}

setTimeout(() => {
    fs.mkdirSync(mkdir, { recursive:true })
    fs.writeFileSync(filedir, content)
    fs.writeFileSync(path.resolve(cwd, outputName, 'README.md'), readme)
    cpx.copySync('./bundle/**', outputPath + '/bundle')
    cpx.copySync('./dist/**', outputPath + '/dist')
    cpx.copySync('./helpers/**', outputPath + '/helpers')
    cpx.copySync('./app.js', outputPath)
}, 2500)
