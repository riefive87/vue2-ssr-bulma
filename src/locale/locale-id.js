export default {
    attributes: {
        email: 'email',
        password: 'kata sandi',
        remember: 'ingatkan saya'
    },
    placeholders: {
        email: 'isi alamat email',
        password: 'isi kata sandi'
    },
    labels: {
        home: 'beranda',
        about: 'tentang',
        loading: 'loading...',
        login: 'masuk',
        logout: 'keluar',
        register: 'daftar',
        refresh: 'muat ulang',
        cancel: 'batal'
    },
    titles: {
        login: 'form login',
        register: 'form daftar'
    },
    text: {
        login: 'Tidak mempunyai akun. Silahkan',
        register: 'Sudah punya akun. Silahkan'
    }
}
