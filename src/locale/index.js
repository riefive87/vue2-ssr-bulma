import Vue from 'vue'
import VueI18n from 'vue-i18n'
import LocaleEn from './locale-en'
import LocaleId from './locale-id'

Vue.use(VueI18n)

const i18n = new VueI18n({
    locale: 'id',
    fallbackLocale: 'en',
    messages: {
        en: LocaleEn,
        id: LocaleId
    }
})

export default i18n
