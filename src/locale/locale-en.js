export default {
    attributes: {
        email: 'email',
        password: 'password',
        remember: 'remember me'
    },
    placeholders: {
        email: 'your email address',
        password: 'your password'
    },
    labels: {
        home: 'home',
        about: 'about',
        loading: 'menunggu...',
        login: 'login',
        logout: 'logout',
        register: 'register',
        refresh: 'refresh',
        cancel: 'cancel'
    },
    titles: {
        login: 'login form',
        register: 'register form'
    },
    text: {
        login: 'Don\`t have an account. Please',
        register: 'I have an account. Please'
    }
}
