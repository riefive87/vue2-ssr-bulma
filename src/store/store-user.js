export default {
    namespaced: true,
    state: {
        loggedIn: false,
        remember: false,
        token: null
    },
    mutations: {
        changeLogin(state, value) {
            state.loggedIn = value
        },
        changeRemember(state, value) {
            state.remember = value
        },
        changeToken(state, value) {
            state.token = value
        }
    },
    actions: {
        updateLogin({ commit }, value) {
            commit('changeLogin', value)
        },
        updateRemember({ commit }, value) {
            commit('changeRemember', value)
        },
        updateToken({ commit }, value) {
            commit('changeToken', value)
        }
    }
}
