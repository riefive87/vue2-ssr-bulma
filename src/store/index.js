import Vue from 'vue'
import Vuex from 'vuex'
import { storeModules, storePlugins } from './store-an-index'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
    },
    mutations: {
    },
    actions: {
    },
    modules: storeModules,
    plugins: storePlugins
})
