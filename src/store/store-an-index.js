import Cookies from 'js-cookie'
import VuexPersistence from 'vuex-persist'
import ModuleBreakpoint from './store-breakpoint'
import ModuleLayout from './store-layout'
import ModuleUser from './store-user'

let plugins = []
if (process.browser) {
    const vuexLocal = new VuexPersistence({
        storage: window.localStorage,
        modules: ['breakpoint', 'layout', 'user']
    })
    plugins.push(vuexLocal.plugin)
} 

const vuexCookie = new VuexPersistence({
    restoreState: (key) => Cookies.getJSON(key),
    saveState: (key, state) => Cookies.set(key, state, { expires: 3 }),
    modules: ['user']
})
plugins.push(vuexCookie.plugin)

const storeModules = {
    breakpoint: ModuleBreakpoint,
    layout: ModuleLayout,
    user: ModuleUser
}
const storePlugins = plugins

export { storeModules, storePlugins }
