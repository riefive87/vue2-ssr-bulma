export default [
    { path: '/login', name: 'Login', meta: { auth: false }, component: () => import('../views/auth/PageLogin') },
    { path: '/register', name: 'Register', meta: { auth: false }, component: () => import('../views/auth/PageRegister') }
]
