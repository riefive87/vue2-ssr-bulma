import RouteDefault from './route-default'
import RouteAuth from './route-auth'

const routes = [].concat(RouteDefault, RouteAuth)

export default routes
