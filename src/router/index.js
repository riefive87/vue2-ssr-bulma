import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '@/store'
import routes from './route-an-index'

const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function push(location) {
    return originalPush.call(this, location).catch((err) => {
        if (err.name !== 'NavigationDuplicated') { throw err }
    })
}

Vue.use(VueRouter)

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: routes.map(route => ({
        path: route.path,
        name: route.name,
        meta: route.meta,
        component: route.component,
        beforeEnter(to, from, next) {
            const existsAuth = to.matched.some(record => record.meta.auth)
            const loggedIn = store.state.user.loggedIn
            const breakpointName = store.state.breakpoint.name
            const fullName = to.fullPath

            if (['Login', 'Register'].includes(to.name)) {
                store.dispatch('layout/updateName', 'auth-view')
            
            } else {
                store.dispatch('layout/updateName', ['xs', 'sm'].includes(breakpointName) ? 'mobile-view' : 'desktop-view')
            }
            
            if (existsAuth) {
                if (!loggedIn) {
                    const query = fullName === '/' ? {} : { redirect: fullName.replace('/', '') }
                    next({ path: '/login', query })
                } else {
                    next()
                }
            } else if (['Login', 'Register'].includes(to.name)) {
                return loggedIn ? next({ path: '/' }) : next()
            } else {
                next()
            }
        }
    }))
})

export default router
