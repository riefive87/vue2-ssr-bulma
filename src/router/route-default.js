export default [
    { path: '/', name: 'Index', meta: { auth: true }, component: () => import('../views/PageIndex.vue') },
    { path: '/about', name: 'About', meta: { auth: true }, component: () => import('../views/PageAbout.vue') }
]

