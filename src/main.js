import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import i18n from './locale'

// add plugins
import './plugins/plugin-breakpoint.js'
import './plugins/plugin-meta.js'

Vue.config.productionTip = false

export function createApp () {
    const app = new Vue({
        router,
        store,
        i18n,
        render: h => h(App)
    })
    return { app, router, store }
}
