import { createApp } from './main'

// add styles
import '@/assets/style-a-preload.scss'
import '@/assets/style-ellipsis.css'
import '@/assets/style.css'
import '@mdi/font/css/materialdesignicons.min.css'
import 'fontsource-poppins'

const { app, router } = createApp()

router.onReady(() => {
    app.$mount('#app')
})
