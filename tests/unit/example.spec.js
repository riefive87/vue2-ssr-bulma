import { mount, shallowMount, createLocalVue } from '@vue/test-utils'
import VueI18n from 'vue-i18n'
import CardPerson from '@/components/CardPerson.vue'
import PageIndex from '@/views/PageIndex.vue'
import PageAbout from '@/views/PageAbout.vue'
import LocaleEn from '@/locale/locale-en'
import LocaleId from '@/locale/locale-id'

const localVue = createLocalVue()
localVue.use(VueI18n)

const i18n = new VueI18n({
    locale: 'id',
    fallbackLocale: 'en',
    messages: {
        en: LocaleEn,
        id: LocaleId
    }
})

describe('Test views', () => {
    it('renders props.email when passed', () => {
        const email = 'george.bluth@reqres.in'
        const wrapper = shallowMount(CardPerson, {
            propsData: { email }
        })
        expect(wrapper.text()).toMatch(email)
    })
    it('display Home view', () => {
        const wrapper = mount(PageIndex, { propsData: {}, shallow: true, localVue, i18n })
        expect(wrapper.html()).toMatch(/loading/i)
    })
    it('display About view', () => {
        const msg = 'This is an about page'
        const wrapper = mount(PageAbout, { propsData: {}, shallow: true, localVue, i18n })
        expect(wrapper.html()).toContain(msg)
    })
})
